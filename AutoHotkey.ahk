; IMPORTANT INFO ABOUT GETTING STARTED: Lines that start with a
; semicolon, such as this one, are comments.  They are not executed.

; This script has a special filename and path because it is automatically
; launched when you run the program directly.  Also, any text file whose
; name ends in .ahk is associated with the program, which means that it
; can be launched simply by double-clicking it.  You can have as many .ahk
; files as you want, located in any folder.  You can also run more than
; one .ahk file simultaneously and each will get its own tray icon.

; SAMPLE HOTKEYS: Below are two sample hotkeys.  The first is Win+Z and it
; launches a web site in the default browser.  The second is Control+Alt+N
; and it launches a new Notepad window (or activates an existing one).  To
; try out these hotkeys, run AutoHotkey again, which will load this file.

#z::Run www.autohotkey.com

^!n::
IfWinExist Untitled - Notepad++
	WinActivate
else
	Run Notepad++
return



; Note: From now on whenever you run AutoHotkey directly, this script
; will be loaded.  So feel free to customize it to suit your needs.

; Please read the QUICK-START TUTORIAL near the top of the help file.
; It explains how to perform common automation tasks such as sending
; keystrokes and mouse clicks.  It also explains more about hotkeys.


;; -- User mod past this point!

; Ctrl+Win+Alt+Space
; Singles out an image in a webpage
^#!Space::
  Click right
  Send i
  Send ^{Tab}
return

; Win+\
; turn off the monitor
#\::
    SendMessage 0x112, 0xF170, 2, , Program Manager  ; Monitor off
    Return

; Win+Shift+\
;turn off the monitor and lock the screen
#+\::
    Run rundll32.exe user32.dll`,LockWorkStation     ; Lock PC
    Sleep 1000
    SendMessage 0x112, 0xF170, 2, , Program Manager  ; Monitor off
    Return

; Win+s
; put comp to sleep
#s::
   ; Parameter #1: Pass 1 instead of 0 to hibernate rather than suspend.
   ; Parameter #2: Pass 1 instead of 0 to suspend immediately rather than asking each application for permission.
   ; Parameter #3: Pass 1 instead of 0 to disable all wake events.
   DllCall("PowrProf\SetSuspendState", "int", 0, "int", 1, "int", 0)
   return

; Win+Shift+s
; put comp to sleep after 30 mins
#+s::
   mins = 30
   secs = mins * 60
   millis = secs * 1000
   MsgBox, 1, Sleep Mode, 
   (
   Sleep Mode Enabled. `nPress "OK" to continue. `n`nTimer set for 30 mins.
   )
;    - Pressing 'Ok' will lock in sleep mode. 
;      NOTE: Pressing 'OK' will reset the timer.
;    - Pressing 'Cancel' will abort the sleep. 
;    - Leaving the box open will execute sleep 
;      mode without locking in the choice.
;   )
   IfMsgBox, Cancel
      return
   IfMsgBox, OK
      sleep 1800000

   DllCall("PowrProf\SetSuspendState", "int", 0, "int", 1, "int", 0)
return

; sound device switcher
SelectDevice(Device,Mode = "P") {
    SetBatchLines -1
    DetectHiddenWindows On
    If Mode = P
        Run rundll32.exe shell32.dll`,Control_RunDLL mmsys.cpl`,`,0
    Else If Mode = R
        Run rundll32.exe shell32.dll`,Control_RunDLL mmsys.cpl`,`,1
    Else
      {
        TrayTip,, Invalid parameter.`nValid options are "P" for playback or "R" for recording., 10, 17
        Return
      }
    WinWait Sound ahk_class #32770
    WinHide
    ControlGet List, List,, SysListView321
    StringReplace List, List, `t, %A_Space%, A
    IfNotInString List, %Device%
      {
        TrayTip,, %Device% not found., 10, 17
        ControlSend Cancel, {Enter}
        Return
      }
    Loop Parse, List, `n
        IfInString A_LoopField, %Device%
          {
            If RegExMatch(A_LoopField, "Default Device$")
              {
                TrayTip,, %Device% is already set as default., 10, 17
                ControlSend Cancel, {Enter}
                Return
              }
            If RegExMatch(A_LoopField, "(Disabled$|Disconnected$|Not plugged in$)")
              {
                TrayTip,, %Device% not available., 10, 17
                ControlSend Cancel, {Enter}
                Return
              }
            DeviceNumber := A_Index
            Break
          }
    If DeviceNumber = 1
        ControlSend SysListView321, {Home}
    Else
      {
        DeviceNumber -= 1
        ControlSend SysListView321, {Home}{Down %DeviceNumber%}
      }
    ControlSend &Set Default, {Space}
    ControlSend OK, {Enter}
    Return
}

;Ctrl+Shift+F9
^+F9::SelectDevice("Speakers Logitech G930 Headset")

^+F10::SelectDevice("S235HL-1 NVIDIA High Definition Audio")

^+F11::SelectDevice("Microphone Logitech G930 Headset", "R")

^+F12::SelectDevice("Microphone Samson Meteor Mic", "R")